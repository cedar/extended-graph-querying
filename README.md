# Extended Queries (EQs): Integrating Connection Search within Graph Queries

This repository contains the code for processing extended queries (EQs). It has the implementation for the EQ execution algorithms proposed in "_Integrating Connecting Search in Graph Queries_", ICDE 2023. We provide the following: 

  1. The executable jar file `eql.jar`.

  2. The `datasets` folder which contains our synthetic RDF benchmarks (line, comb, star, Barabasi-Albert and CDF graphs).

  3. The `scripts` folder with scripts to execute EQs on the datasets. 
   
  4. The `settings` folder with settings to be used with the jar for various datasets.
   
 

## Software prerequisites
Required: 
- Java 11 (tested with openjdk version "11.0.12")
- PostgreSQL (tested with v.12.4)
- Python 3.6.5

Optional: 

- [Graphviz (DOT)](https://www.graphviz.org/) 


##  Usage instructions

The jar can be used in conjunction with the scripts in the `scripts` folder. The parameter values can be changed in the settings file to choose the desired algorithm. The purpose of each parameter and its possible values are described in the settings file itself.

QGSTP's DBPedia dataset can be generated using the code of the authors available at https://github.com/nju-websoft/QGSTP.

YAGO3 data can be downloaded from https://yago-knowledge.org/downloads/yago-3.

## About

This work is a part of the ConnectionLens project (https://team.inria.fr/cedar/connectionlens/) which is not open-source yet and hence, we provide an executable jar for our work.

## Support
For any help or to report errors, please file an issue in the project.

## Authors

- Angelos Christos Anadiotis (Oracle, Switzerland)
- Ioana Manolescu (Inria and IPP, France)
- Madhulika Mohanty (Inria and IPP, France)
